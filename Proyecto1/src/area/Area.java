package area;

import java.util.Scanner;

public class Area {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		
		System.out.println("Introduce el radio del c�rculo");
		double radio=input.nextDouble();
		
		System.out.println("El �rea del c�rculo es: "+(Math.PI*(radio*radio)));
		
		input.close();

	}

}
